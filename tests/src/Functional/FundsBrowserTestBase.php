<?php

namespace Drupal\Tests\commerce_funds\Functional;

use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;
use Drupal\Tests\commerce_funds\Traits\FundsTrait;

/**
 * Provides a base class for Commerce funds functional tests.
 */
abstract class FundsBrowserTestBase extends CommerceBrowserTestBase {

  use FundsTrait;

  /**
   * Set default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'commerce_funds',
    'commerce_exchanger',
  ];

  /**
   * The product manager.
   *
   * @var \Drupal\commerce_funds\ProductManagerInterface
   */
  protected $productManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser($this->getAdministratorPermissions() + [
      'administer commerce_order',
      'administer commerce_payment',
      'access commerce_order overview',
    ], NULL, TRUE);

    $this->paymentGateway = $this->createEntity('commerce_payment_gateway', [
      'id' => 'manual',
      'label' => 'Manual',
      'plugin' => 'manual',
      'configuration' => [
        'collect_billing_information' => FALSE,
      ],
      'status' => 1,
    ]);

    $this->transactionManager = $this->container->get('commerce_funds.transaction_manager');
    $this->productManager = $this->container->get('commerce_funds.product_manager');
    $this->configFactory = $this->container->get('config.factory');
    $this->webAssert = $this->assertSession();
  }

}
