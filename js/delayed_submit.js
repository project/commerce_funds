/**
 * @file
 * Delayed form submit.
 */
((Drupal) => {
  /**
   * Behaviors to automatically submit a form after a delay.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches delayed submit behavior to conversion transactions.
   */
  Drupal.behaviors.delayed_submit = {
    attach(context) {
      once('calculateFees', 'body', context).forEach(() => {
        const delayedInputSubmitElements = document.querySelectorAll(
          '.delayed-input-submit',
        );

        const createTimeout = (element, delay, triggerEvent) => {
          setTimeout(() => {
            if (element.value) {
              const event = new Event(triggerEvent);
              element.dispatchEvent(event);
            }
          }, delay);
        };

        delayedInputSubmitElements.forEach((element) => {
          let timeout = null;
          const delay = parseInt(element.getAttribute('data-delay'), 4) || 1000;
          const triggerEvent =
            element.getAttribute('data-event') || 'end_typing';

          const keyupHandler = () => {
            clearTimeout(timeout);
            timeout = createTimeout(element, delay, triggerEvent);
          };

          element.removeEventListener('keyup', keyupHandler);
          element.addEventListener('keyup', keyupHandler);
        });
      });
    },
  };
})(Drupal);
