# Commerce Funds

The Commerce Funds module is a Drupal Commerce extension that implements a Funds Management System for your website. It creates Account Balances to hold "virtual money" in them, and it allows users to Deposit Funds, Transfer Funds, make Escrow Payments, Buy any product on the site using user balances, and send Withdrawal Requests to administrators. This module works with multi-currency websites.

## Features

* Account Balances to hold "virtual money"
* Deposit Funds
* Transfer Funds
* Escrow Payments
* Buy any product on the site using user balances
* Withdrawal Requests to administrators
* Multi-currency support (currency conversion)

## Configuration

The module provides a set of configuration options that allow you to customize its behavior. These options can be found in the Drupal admin interface under Configuration > Commerce > Funds.

## Usage

Commerce Funds provides virtual money transactions that need to be executed manually by site administrators. It's important to understand that you don't actually host any money on your website for security reasons.

### FAQs

* **Why deposits are not added to my user balance?**

  If you're using the "manual" payment gateway, completing an order does not automatically trigger order payment. To add funds to a user balance, you need to manually inform Drupal Commerce that you have received the cash payment. To trigger the payment, as an administrator, go to Commerce > Orders, select your order, and then choose Receive Payment.

* **Why my user balance is equal to the site balance?**

  The site balance is essentially a user balance, specifically the balance of user 1 (the super administrator). This user account should not be used for testing, as it will accumulate transaction fees in addition to the transaction amounts.

* **How to add money to users' balances (or the site balance)?**

  Commerce Funds is designed to operate in isolation. You cannot create money out of nothing. The only way to add money to an account is by depositing funds using one of the payment methods available on your website.

* **Using the currency converter**

  For every currency you will enable on your store, starting from 2 currencies, you can enable currency conversion by using Commerce Exchanger. You can choose to let a third party API keeping track of the currency exchange rates or set them manually yourself.

* **Funds balance as a payment method**

  If you want to give the possibility to your users to pay products from your website using their account balance, you can configure a new payment gateway. Select "Funds Balance", set the mode to live, and then restrict the payment gateway to the options you want. Be aware that you shouldn't allow users to pay a deposit using their balance account, as it doesn't make any sense.

## Documentation

You can find detailed documentation on how to use this module on the [Drupal website](https://www.drupal.org/docs/contributed-modules/commerce-funds/how-to-use).

## Documentation for developers

Developers can find documentation on how to use the module's services and entities on the [Drupal website](https://www.drupal.org/docs/8/modules/commerce-funds/tools-for-developers).

## Credits

This module is developed and maintained by [Orao-web](https://orao-web.net/).

## License

The Commerce Funds module is licensed under the GNU General Public License version 2.0. See the LICENSE file for more information.

## Links

* [Drupal module page](https://www.drupal.org/project/commerce_funds)
* [Drupal documentation](https://www.drupal.org/docs/contributed-modules/commerce-funds)
* [Orao-web](https://orao-web.net/)
