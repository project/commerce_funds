<?php

namespace Drupal\commerce_funds\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a WithdrawalMethod attribute for plugin discovery.
 *
 * Withdrawal method plugins allow to set a configuration
 * for a specific withdrawal method.
 *
 * Additional attribute keys for withdrawalMethod can be defined in
 * hook_commerce_funds_withdrawal_methods_info_alter().
 *
 * @see \Drupal\commerce_funds\WithdrawalMethodPluginManager
 * @see \Drupal\commerce_funds\WithdrawalMethodPluginManagerInterface
 *
 * @ingroup funds_withdrawal_method
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class WithdrawalMethod extends Plugin {

  /**
   * Constructs a WithdrawalMethod attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   (optional) The human-readable name of the withdrawal method.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
  ) {}

}
