<?php

namespace Drupal\commerce_funds\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a withdrawal method item annotation object.
 *
 * Plugin Namespace: Plugin\commerce_funds\WithdrawalMethod.
 *
 * @see \Drupal\commerce_funds\Plugin\WithdrawalMethodPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class WithdrawalMethod extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the withdrawal method.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
