<?php

namespace Drupal\commerce_funds\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The commerce funds settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $fundsSettings;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->fundsSettings = $config_factory->get('commerce_funds.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $config = $this->fundsSettings->get('global');

    if (!empty($config['disable_funds_forms'])) {
      foreach ($config['disable_funds_forms'] as $form_name) {
        if ($form_name) {
          if ($collection->get('commerce_funds.' . $form_name)) {
            $collection->remove(['commerce_funds.' . $form_name]);
          }
        }
      }
    }
  }

}
