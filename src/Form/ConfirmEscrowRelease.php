<?php

namespace Drupal\commerce_funds\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\commerce_funds\Entity\Transaction;
use Drupal\commerce_funds\TransactionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines a confirmation form to release an escrow payment.
 */
class ConfirmEscrowRelease extends ConfirmFormBase {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The transaction manager.
   *
   * @var \Drupal\commerce_funds\TransactionManagerInterface
   */
  protected $transactionManager;

  /**
   * The transaction.
   *
   * @var \Drupal\commerce_funds\Entity\Transaction
   */
  protected $transaction;

  /**
   * Class constructor.
   */
  public function __construct(AccountProxy $current_user, TransactionManagerInterface $transaction_manager) {
    $this->currentUser = $current_user;
    $this->transactionManager = $transaction_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('commerce_funds.transaction_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_escrow_release";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('view.commerce_funds_user_transactions.incoming_escrow_payments');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to release that escrow payment?');
  }

  /**
   * Check if the user is allowed to perform an escrow operation.
   *
   * @param \Drupal\commerce_funds\Entity\Transaction $transaction
   *   The transaction id to check permissions on.
   *
   * @return bool
   *   User is allowed or not.
   */
  protected function isUserAllowed(Transaction $transaction) {
    $uid = $this->currentUser->id();

    if ($transaction->getStatus() === $transaction::TRANSACTION_STATUS['pending']) {
      if ($uid == $transaction->getIssuerId()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $transaction_hash = NULL) {
    $this->transaction = $this->transactionManager->loadTransactionByHash($transaction_hash);
    // Check if the user is allowed to perform the operation.
    if ($this->transaction && $this->isUserAllowed($this->transaction)) {
      $currency_code = $this->transaction->getCurrencyCode();
      $currency_symbol = $this->transaction->getCurrency()->getSymbol();
      $amount = $this->transaction->getNetAmount();
      $form['transaction'] = ['#markup' => $this->t('<h2>Transaction details</h2>')];
      $form += [
        'recipient' => [
          '#markup' => $this->t('Recipient: <a href="@recipient-url">@recipient</a> <br>', [
            '@recipient-url' => Url::fromRoute('entity.user.canonical', ['user' => $this->transaction->getRecipientId()])->toString(),
            '@recipient' => $this->transaction->getRecipient()->getAccountName(),
          ]),
        ],
        'amount' => [
          '#markup' => $this->t('Amount: @currency_symbol@amount @currency_code <br>', [
            '@amount' => $amount,
            '@currency_code' => $currency_code,
            '@currency_symbol' => $currency_symbol,
          ]),
        ],
      ];
      return parent::buildForm($form, $form_state);
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $transaction = $this->transaction;

    // Send emails.
    $this->transactionManager->sendTransactionMails($transaction);
    // Release escrow payment.
    $this->transactionManager->addFundsToBalance($transaction, $transaction->getRecipient());
    // Update site balance.
    $this->transactionManager->updateSiteBalance($transaction);
    // Update transaction status.
    $transaction->setStatus($transaction::TRANSACTION_STATUS['completed']);
    $transaction->save();

    // Generate confirmation message.
    $this->transactionManager->generateConfirmationMessage($transaction);

    // Set redirection.
    $form_state->setRedirect('view.commerce_funds_user_transactions.incoming_escrow_payments');
  }

}
