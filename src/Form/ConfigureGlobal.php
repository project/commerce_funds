<?php

namespace Drupal\commerce_funds\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure global settings.
 */
class ConfigureGlobal extends ConfigFormBase {

  /**
   * The route builder.
   *
   * @var \Drupal\Core\ProxyClass\Routing\RouteBuilder
   */
  protected $routeBuilder;

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Class constructor.
   */
  public function __construct(RouteBuilder $route_builder, CacheBackendInterface $cache_render) {
    $this->routeBuilder = $route_builder;
    $this->cacheRender = $cache_render;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder'),
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_funds_configure_global';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_funds.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_funds.settings')->get('global');

    $form['global'] = [
      '#type' => 'details',
      '#title' => $this->t('Default forms'),
      '#open' => TRUE,
    ];

    $options = [
      'deposit' => $this->t('Deposit form'),
      'transfer' => $this->t('Transfer form'),
      'escrow' => $this->t('Escrow form'),
      'convert_currencies' => $this->t('Currency converter form'),
      'withdraw' => $this->t('Withdrawal form'),
    ];

    $form['global']['disable_funds_forms'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disable default forms?'),
      "#description" => $this->t('In case you are using Transaction fields, this will disable the routes such as /user/funds/deposit or /user/funds/transfer etc.'),
      '#default_value' => $config['disable_funds_forms'] ? array_keys(array_filter($config['disable_funds_forms'])) : [],
      '#options' => $options,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $this->config('commerce_funds.settings')
      ->set('global.disable_funds_forms', $values['disable_funds_forms'])
      ->save();

    // Rebuild routes.
    $this->routeBuilder->rebuild();
    // Clear cache.
    $this->cacheRender->invalidateAll();

    parent::submitForm($form, $form_state);
  }

}
