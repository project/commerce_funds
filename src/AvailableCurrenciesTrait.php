<?php

namespace Drupal\commerce_funds;

use Drupal\Core\Form\FormStateInterface;

/**
 * Allows field types to limit the available currencies.
 */
trait AvailableCurrenciesTrait {

  /**
   * A list of available currencies.
   *
   * @var array
   */
  protected static $availableCurrencies = [];

  /**
   * Defines the default field-level settings.
   *
   * @return array
   *   A list of default settings, keyed by the setting name.
   */
  public static function defaultCurrencySettings() {
    return [
      'available_currencies' => [],
    ];
  }

  /**
   * Builds select element form.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The currency select element.
   */
  public function currencySelectForm(array $form, FormStateInterface $form_state) {
    $currencies = \Drupal::entityTypeManager()->getStorage('commerce_currency')->loadMultiple();
    $currency_codes = [];
    /** @var \Drupal\commerce_price\Entity\Currency $currency */
    foreach ($currencies as $currency) {
      $currency_codes[$currency->getCurrencyCode()] = $currency->getCurrencyCode();
    }
    // Make sure currencies are sorted.
    ksort($currencies);

    return [
      '#type' => 'select',
      '#title' => $this->t('Select Currency'),
      '#description' => $this->t('Select a currency.'),
      '#options' => $currency_codes,
    ];
  }

}
