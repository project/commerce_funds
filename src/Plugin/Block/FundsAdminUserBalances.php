<?php

namespace Drupal\commerce_funds\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\commerce_funds\TransactionManagerInterface;
use Drupal\commerce_price\Entity\Currency;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an admin block for user balances.
 */
#[Block(
  id: "admin_user_balances",
  admin_label: new TranslatableMarkup("Admin user balances"),
  context_definitions: [
    "user" => new EntityContextDefinition(
      data_type: "entity:user",
      label: new TranslatableMarkup("User"),
    ),
  ]
)]
class FundsAdminUserBalances extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The transaction manager.
   *
   * @var \Drupal\commerce_funds\TransactionManagerInterface
   */
  protected $transactionManager;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, TransactionManagerInterface $transaction_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->transactionManager = $transaction_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('commerce_funds.transaction_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'administer transactions');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $account = $this->getContextValue('user');
    $balance = $this->transactionManager->loadAccountBalance($account);
    foreach ($balance as $currency_code => $amount) {
      $symbol = Currency::load($currency_code)->getSymbol();
      $balance[$currency_code] = $symbol . $amount;
    }

    return [
      '#theme' => 'admin_user_balances',
      '#balance' => $balance ?: 0,
      '#cache' => [
        'tags' => $this->getCacheTags(),
        'contexts' => $this->getCacheContexts(),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $account = $this->getContextValue('user');
    return Cache::mergeTags(parent::getCacheTags(), ['funds_balance:' . $account->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // We purposely set a cache context to route
    // and not route.user, so the block can be unvalidated
    // on non user routes, giving wider choices to site builders.
    // @see getContextValue().
    return Cache::mergeContexts(parent::getCacheContexts(), ['user', 'route']);
  }

  /**
   * {@inheritdoc}
   */
  public function getContextValue($name) {
    if ($name === 'user') {
      if ($user = $this->routeMatch->getParameter('user')) {
        return $user;
      }
      // The route doesn't implement parameters.
      if (!$user instanceof AccountInterface && is_numeric($user)) {
        return User::load($user);
      }
    }
    return parent::getContextValue($name);
  }

}
