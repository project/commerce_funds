<?php

namespace Drupal\commerce_funds\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Issuer equals current user constraint.
 */
#[Constraint(
  id: "IssuerEqualsCurrentUser",
  label: new TranslatableMarkup("Issuer equals current user.", [], ["context" => "Validation"])
)]
class IssuerEqualsCurrentUserConstraint extends SymfonyConstraint {
  /**
   * {@inheritdoc}
   */
  public $message = "Operation impossible. You can't transfer money to yourself.";

}
