<?php

namespace Drupal\commerce_funds\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * User funds validation constraint.
 */
#[Constraint(
  id: "NetAmountBelowBalance",
  label: new TranslatableMarkup("Net amount is superior to balance amount.", [], ["context" => "Validation"])
)]
class NetAmountBelowBalanceConstraint extends SymfonyConstraint {

  /**
   * {@inheritdoc}
   */
  public $message = "Not enough funds to cover this transfer.";

  /**
   * {@inheritdoc}
   */
  public $messageWithFee = "Not enough funds to cover this transfer (Total: %total (@currency).";

}
