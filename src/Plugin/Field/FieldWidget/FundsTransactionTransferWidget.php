<?php

namespace Drupal\commerce_funds\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Funds transaction transfer field widget.
 */
#[FieldWidget(
  id: "commerce_funds_transaction_transfer",
  label: new TranslatableMarkup("Transfer"),
  field_types: ["commerce_funds_transaction"],
)]
class FundsTransactionTransferWidget extends FundsTransactionWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['target_id']['#transaction_type'] = 'transfer';

    return $element;
  }

}
