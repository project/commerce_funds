<?php

namespace Drupal\commerce_funds\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\commerce_funds\Entity\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Funds transaction field formatter.
 */
#[FieldFormatter(
  id: "commerce_funds_transaction",
  label: new TranslatableMarkup("Transaction"),
  field_types: [
    "commerce_funds_transaction",
  ],
)]
class FundsTransactionTransferFormatter extends FormatterBase {

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * The formatter settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * The label display setting.
   *
   * @var string
   */
  protected $label;

  /**
   * The view mode.
   *
   * @var string
   */
  protected $viewMode;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, Renderer $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $source = [];
    // Render output using field__funds_transaction theme.
    if ($items->first()) {
      $transaction = Transaction::load($items->first()->getValue()['target_id']);

      $source = [
        '#theme' => 'field__funds_transaction',
        '#issuer' => $transaction->getIssuer()->getAccountName(),
        '#recipient' => $transaction->getRecipient()->getAccountName(),
        '#method' => $transaction->getMethod(),
        '#brut_amount' => $transaction->getBrutAmount(),
        '#net_amount' => $transaction->getNetAmount(),
        '#fee' => $transaction->getFee(),
        '#currency_symbol' => $transaction->getCurrency()->getSymbol(),
        '#currency_code' => $transaction->getCurrencyCode(),
        '#status' => $transaction->getStatus(),
        '#notes' => Markup::create($transaction->getNotes()),
      ];
    }

    $elements[0] = ['#markup' => $this->renderer->render($source)];

    return $elements;
  }

}
