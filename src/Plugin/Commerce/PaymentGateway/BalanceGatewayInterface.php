<?php

namespace Drupal\commerce_funds\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsCreatingPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;

/**
 * Provides the interface for the Funds balance payment gateway.
 */
interface BalanceGatewayInterface extends PaymentGatewayInterface, SupportsStoredPaymentMethodsInterface, SupportsCreatingPaymentMethodsInterface {

}
