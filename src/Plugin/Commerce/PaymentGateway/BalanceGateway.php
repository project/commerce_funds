<?php

namespace Drupal\commerce_funds\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\commerce_funds\Entity\Transaction;
use Drupal\commerce_funds\PluginForm\Funds\BalanceMethodAddForm;
use Drupal\commerce_payment\Attribute\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Balance payment gateway.
 */
#[CommercePaymentGateway(
  id: "funds_balance",
  label: new TranslatableMarkup("Funds balance"),
  display_label: new TranslatableMarkup("Funds balance"),
  forms: [
    "add-payment-method" => BalanceMethodAddForm::class,
    "edit-payment-method" => BalanceMethodAddForm::class,
  ],
  payment_method_types: ["funds_wallet"],
  requires_billing_information: FALSE,
)]
class BalanceGateway extends PaymentGatewayBase implements BalanceGatewayInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The transaction manager.
   *
   * @var \Drupal\commerce_funds\TransactionManager
   */
  protected $transactionManager;

  /**
   * The fees manager.
   *
   * @var \Drupal\commerce_funds\FeesManagerInterface
   */
  protected $feesManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    $instance->transactionManager = $container->get('commerce_funds.transaction_manager');
    $instance->feesManager = $container->get('commerce_funds.fees_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['mode']['#access'] = FALSE;
    $form['mode']['#default_value'] = 'live';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    parent::assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    parent::assertPaymentMethod($payment_method);

    $this->doPayment($payment_method, $payment);
    $payment->setState('completed');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = ['balance_id', 'currency'];

    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }
    $payment_method->balance_id = $payment_details['balance_id'];
    $payment_method->currency = $payment_details['currency'];

    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * Performs the payment operation.
   *
   * Remove total price from user balance and update site balance.
   *
   * @see createPaymentMethod()
   */
  protected function doPayment(PaymentMethodInterface $payment_method, PaymentInterface $payment) {
    $order = $this->routeMatch->getParameter('commerce_order');
    // Allows to perform payment
    // outside of the order route when needed.
    if (empty($order)) {
      $order = $payment->getOrder();
    }

    foreach ($order->getItems() as $item) {
      $fee = $this->feesManager->calculateTransactionFee($item->getTotalPrice()->getNumber(), $item->getTotalPrice()->getCurrencyCode(), 'payment');
      $transaction = Transaction::create([
        'issuer' => $payment_method->getOwnerId(),
        'recipient' => $item->getPurchasedEntity()->getOwnerId(),
        'type' => 'payment',
        'method' => $payment_method->bundle(),
        'brut_amount' => $item->getTotalPrice()->getNumber(),
        'net_amount' => $fee['net_amount'],
        'fee' => $fee['fee'],
        'currency' => $item->getTotalPrice()->getCurrencyCode(),
        'status' => Transaction::TRANSACTION_STATUS['canceled'],
        'notes' => [
          'value' => $this->t('Payment of <a href="@product-url">@product-name</a> (order <a href="@order-url">#@order-id</a>)', [
            '@product-url' => Url::fromRoute('entity.commerce_product.canonical', [
              'commerce_product' => $item->getPurchasedEntityId(),
            ])->toString(),
            '@product-name' => $item->getTitle(),
            '@order-url' => Url::fromRoute('entity.commerce_order.user_view', [
              'user' => $payment_method->getOwnerId(),
              'commerce_order' => $payment->getOrderId(),
            ])->toString(),
            '@order-id' => $payment->getOrderId(),
          ]),
          'format' => 'basic_html',
        ],
      ]);
      $transaction->save();

      $this->transactionManager->performTransaction($transaction);
    }
  }

}
