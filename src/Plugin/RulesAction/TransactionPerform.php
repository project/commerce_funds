<?php

namespace Drupal\commerce_funds\Plugin\RulesAction;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\commerce_funds\Entity\Transaction;
use Drupal\commerce_funds\FeesManagerInterface;
use Drupal\commerce_funds\TransactionManagerInterface;
use Drupal\commerce_price\Calculator;
use Drupal\rules\Context\ContextDefinition;
use Drupal\rules\Core\Attribute\RulesAction;
use Drupal\rules\Core\RulesActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Perform the transaction.
 */
#[RulesAction(
  id: "commerce_funds_perform_transaction",
  label: new TranslatableMarkup("Perform transaction"),
  category: new TranslatableMarkup("Transaction"),
  provider: "ban",
  context_definitions: [
    "transaction" => new ContextDefinition(
      data_type: "entity:commerce_funds_transaction",
      label: new TranslatableMarkup("Transaction"),
      description: new TranslatableMarkup("Specifies the transaction that should be performed."),
    ),
  ]
)]
class TransactionPerform extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The fees manager.
   *
   * @var \Drupal\commerce_funds\FeesManagerInterface
   */
  protected $feesManager;

  /**
   * The transaction manager.
   *
   * @var \Drupal\commerce_funds\TransactionManagerInterface
   */
  protected $transactionManager;

  /**
   * Constructs the RulesBanActionBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_funds\FeesManagerInterface $fees_manager
   *   The fees manager.
   * @param \Drupal\commerce_funds\TransactionManagerInterface $transaction_manager
   *   The transaction manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FeesManagerInterface $fees_manager, TransactionManagerInterface $transaction_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->feesManager = $fees_manager;
    $this->transactionManager = $transaction_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_funds.fees_manager'),
      $container->get('commerce_funds.transaction_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function refineContextDefinitions(array $selected_data) {
    if ($selected_data && isset($selected_data['transaction'])) {
      $type = $selected_data['transaction']->getDataType();
      $this->getPluginDefinition()['context_definitions']['transaction']->setDataType($type);
    }
  }

  /**
   * Perform transaction.
   *
   * @param \Drupal\commerce_funds\Entity\Transaction $transaction
   *   The transaction to be performed.
   */
  protected function doExecute(Transaction $transaction) {
    // Fee was set in the rules.
    if ($transaction->getFee()) {
      $transaction->setFee($transaction->getFee());
      $transaction->setNetAmount($this->addRulesFee($transaction->getBrutAmount(), $transaction->getFee()));
    }
    // No Rule fee, we use fees set in config.
    else {
      $fee_applied = $this->feesManager->calculateTransactionFee($transaction->getBrutAmount(), $transaction->getCurrencyCode(), $transaction->bundle());
      $transaction->setFee($fee_applied['fee']);
      $transaction->setNetAmount($fee_applied['net_amount']);
    }

    $transaction->save();
    $this->transactionManager->performTransaction($transaction);
  }

  /**
   * Apply rule fee to brut amount.
   *
   * @param float $brut_amount
   *   The amount entered by the user.
   * @param float $fee
   *   The fee set in the rule.
   *
   * @return string
   *   The net amount of the transaction.
   */
  protected function addRulesFee($brut_amount, $fee) {
    return Calculator::add((string) $brut_amount, (string) $fee);
  }

}
